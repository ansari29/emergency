class Emergencies:

    def __init__(self,name,address) -> None:
        print('Please Select The Emergency:\nPress 1 for Fire Department\nPress 2 for Hospital\nPress 3 for Paramedics\nPress 4 for police department\nPress 5 for other Emergencies')

        self.name = name
        self.address = address

    def fire_department(self):
        return f'Fire Department will reach shortly on {self.address} '

    def hospital(self):
        return f'{self.name} your appointment has been made thank you for contacting us'

    def paramedics(self):
        return f'paramedics will reach out {self.name} dont panic'

    def police_deprtment(self):
        return f'Thank you for contacting us {self.name},A team has been dispatched at your current location: {self.address}'

    def other(self):
        return f'Our team will shortly visit you {self.name} at your current address: {self.address}'

class Details(Emergencies):

    def choose_emergency(self,opt):
        if opt == 1:
            return self.fire_department ()
        elif opt == 2:
            return self.hospital()
        elif opt == 3:
            return self.paramedics()
        elif opt == 4:
            return self.police_deprtment()    
        elif opt == 5:
            return self.other()     

person = Details('Jane','Groove street')
print(person.choose_emergency(3))                          
